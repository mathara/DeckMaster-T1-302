import java.util.ArrayList;
import java.util.Random;

/**
* Esta classe representa um Jogador aleatório (realiza jogadas de maneira aleatória) para o jogo LaMa (Lacaios & Magias).
* @see java.lang.Object
* @author Matheus Mendes Araujo - MC302
*/
public class JogadorControle extends Jogador {
	private ArrayList<CartaLacaio> lacaios;
	private ArrayList<CartaLacaio> lacaiosOponente;
	
	/**
	  * O método construtor do JogadorAleatorio.
	  * 
	  * @param maoInicial Contém a mão inicial do jogador. Deve conter o número de cartas correto dependendo se esta classe Jogador que está sendo construída é o primeiro ou o segundo jogador da partida. 
	  * @param primeiro   Informa se esta classe Jogador que está sendo construída é o primeiro jogador a iniciar nesta jogada (true) ou se é o segundo jogador (false).
	  */
	public JogadorControle(ArrayList<Carta> maoInicial, boolean primeiro){
		primeiroJogador = primeiro;
		
		mao = maoInicial;
		lacaios = new ArrayList<CartaLacaio>();
		lacaiosOponente = new ArrayList<CartaLacaio>();
		
		// Mensagens de depuração:
		System.out.println("*Classe JogadorRA156737* Sou o " + (primeiro?"primeiro":"segundo") + " jogador (classe: JogadorAleatorio)");
		System.out.println("Mao inicial:");
		for(int i = 0; i < mao.size(); i++)
			System.out.println("ID " + mao.get(i).getID() + ": " + mao.get(i));
	}
	
	/**
	  * Um método que processa o turno de cada jogador. Este método deve retornar as jogadas do Jogador decididas para o turno atual (ArrayList de Jogada).
	  * 
	  * @param mesa   O "estado do jogo" imediatamente antes do início do turno corrente. Este objeto de mesa contém todas as informações 'públicas' do jogo (lacaios vivos e suas vidas, vida dos heróis, etc).
	  * @param cartaComprada   A carta que o Jogador recebeu neste turno (comprada do Baralho). Obs: pode ser null se o Baralho estiver vazio ou o Jogador possuir mais de 10 cartas na mão.
	  * @param jogadasOponente   Um ArrayList de Jogada que foram os movimentos utilizados pelo oponente no último turno, em ordem.
	  * @return            um ArrayList com as Jogadas decididas
	  */
	public ArrayList<Jogada> processarTurno (Mesa mesa, Carta cartaComprada, ArrayList<Jogada> jogadasOponente){
		int minhaMana, minhaVida;
		if(cartaComprada != null)
			mao.add(cartaComprada);
		if(primeiroJogador){
			minhaMana = mesa.getManaJog1();
			minhaVida = mesa.getVidaHeroi1();
			lacaios = mesa.getLacaiosJog1();
			lacaiosOponente = mesa.getLacaiosJog2();
			//System.out.println("--------------------------------- Começo de turno pro jogador1");
		}
		else{
			minhaMana = mesa.getManaJog2();
			minhaVida = mesa.getVidaHeroi2();
			lacaios = mesa.getLacaiosJog2();
			lacaiosOponente = mesa.getLacaiosJog1();
			//System.out.println("--------------------------------- Começo de turno pro jogador2");
		}
		
		ArrayList<Jogada> minhasJogadas = new ArrayList<Jogada>();
		ArrayList<CartaLacaio> meusLacaios = primeiroJogador ? mesa.getLacaiosJog1() : mesa.getLacaiosJog2();
		ArrayList<Carta> LacaiosOrdemMana = new ArrayList<Carta>();
		
		minhasJogadas = Controle ( minhaMana, minhaVida, minhasJogadas,  meusLacaios, LacaiosOrdemMana );
		
		return minhasJogadas;
	}
	
	public int CustoTodosLacaiosOp( ArrayList<CartaLacaio> lacaiosOponente){
		int custoTotal = 0;
		
		for(int i = 0; i<lacaiosOponente.size(); i++){
			CartaLacaio lacopo = lacaiosOponente.get(i);
			custoTotal += lacopo.getMana();
		}
		
		return custoTotal;
	}
	
	public int AtaqueTodosLacaiosOp( ArrayList<CartaLacaio> lacaiosOponente){
		int AtaqueLacaios = 0;
		
		for(int i = 0; i<lacaiosOponente.size(); i++){
			CartaLacaio lacopo = lacaiosOponente.get(i);
			AtaqueLacaios += lacopo.getAtaque();
		}
		
		return AtaqueLacaios;
	}
	
	public int AtaqueMeusLacaios( ArrayList<CartaLacaio> meusLacaios){
		int AtaqueLacaios = 0;
		
		for(int i = 0; i<meusLacaios.size(); i++){
			CartaLacaio lacaio = meusLacaios.get(i);
			AtaqueLacaios += lacaio.getAtaque();
		}
		
		return AtaqueLacaios;
	}
	
	public ArrayList<Jogada> Controle ( int minhaMana, int minhaVida, ArrayList<Jogada> minhasJogadas, 
			ArrayList<CartaLacaio> meusLacaios, ArrayList<Carta> LacaiosOrdemMana ){
		
		// O laço abaixo cria jogadas de baixar LACAIOS se houver mana disponível.
		//laço de selecionar os lacaios mais fortes na frente
		for (int i = 0; i < mao.size(); i++){
			Carta card = mao.get(i);
			if (i == 0){
				if (card instanceof CartaLacaio){
					LacaiosOrdemMana.add(card);
				}
			}
			else{
				if (card instanceof CartaLacaio){
					for (int j = 0, flag = 0; j < LacaiosOrdemMana.size() && flag == 0; j++){
						CartaLacaio lac1 =  (CartaLacaio) LacaiosOrdemMana.get(j);
						CartaLacaio lac2 =  (CartaLacaio) card;
						
						if (lac1.getMana() > lac2.getMana()){
							LacaiosOrdemMana.add(j, card);
							flag = 1;
						}
						else if (lac1.getMana() == lac2.getMana()){
							if (lac1.getAtaque() <= lac2.getAtaque()){
								LacaiosOrdemMana.add(j, card);
							}
							else {
								LacaiosOrdemMana.add(j+1, card);
							}
							flag = 1;
						}
						else if (j == LacaiosOrdemMana.size() - 1 ){
							LacaiosOrdemMana.add(card);
							flag = 1;
						}
					}
				}
			}
		}
		
		System.out.println("Jogada: Decidi baixar o lacaio: "+LacaiosOrdemMana);
		
		//invocar o lacaio
		for (int i = 0; i < LacaiosOrdemMana.size(); i++){
			Carta card = LacaiosOrdemMana.get(i);
			if (meusLacaios.size() < 7){
				//usar carta LACAIO
				if(card instanceof CartaLacaio && card.getMana() <= minhaMana){
					Jogada lac = new Jogada(TipoJogada.LACAIO, card, null);
					minhasJogadas.add(lac);
					minhaMana -= card.getMana();
					System.out.println("Jogada: Decidi baixar o lacaio: "+ card);
					
					for (int j = 0, flag = 0; j < mao.size() && flag == 0; j++){
						if (mao.get(j).getID() == card.getID()){
							mao.remove(j);
							flag = 1;
						}
					}
					LacaiosOrdemMana.remove(i);
					i--;
				}
			}
		}
		
		// O laço abaixo cria jogadas de baixar MAGIAAAS se houver mana disponível.
		for(int i = 0; i < mao.size(); i++){
			Carta card = mao.get(i);
			
			//usar carta MAGIA
			if(card instanceof CartaMagia && card.getMana() <= minhaMana){
				CartaMagia magia = (CartaMagia) card;
				
				if (magia.getMagiaTipo() == TipoMagia.ALVO){
					for (int j = 0,flag = 0; j < lacaiosOponente.size() && flag == 0; j++){
						Carta LacaioOp = lacaiosOponente.get(j);
						CartaLacaio lac1 = (CartaLacaio) LacaioOp;
						if (Math.abs(magia.getMagiaDano() - lac1.getVidaAtual()) == 1 || magia.getMagiaDano() - lac1.getVidaAtual() == 0 ){
							Jogada mag = new Jogada(TipoJogada.MAGIA, card, null);
							minhasJogadas.add(mag);
							minhaMana -= card.getMana();
							if (magia.getMagiaDano() - lac1.getVidaAtual() >= 0 ){
								lacaiosOponente.remove(j);
							}
							System.out.println("Jogada: Olha essa magia na sua cara "+ card);
							mao.remove(i);
							i--;
							flag = 1;
						}
					}
				}
				else if(magia.getMagiaTipo() == TipoMagia.AREA && lacaiosOponente.size() >= 2){
					Jogada mag = new Jogada(TipoJogada.MAGIA, card, null);
					minhasJogadas.add(mag);
					minhaMana -= card.getMana();
					System.out.println("Jogada: Olha essa magia na sua cara "+ card);
					mao.remove(i);
					i--;
					
					//Verifica quem foi eliminado para remover
					for (int j = 0; j < lacaiosOponente.size(); j++){
						if ( magia.getMagiaDano() - lacaiosOponente.get(j).getVidaAtual() >= 0){
							lacaiosOponente.remove(j);
							j--;
						}
					}
					
				}
				else if (magia.getMagiaTipo() == TipoMagia.BUFF){
					if (meusLacaios.size() != 0){
						Carta LacaioVivoBuffar = meusLacaios.get(0);
						for (int j =0; j<meusLacaios.size();j++){
							if (j ==0){
								LacaioVivoBuffar = meusLacaios.get(j);
							}
							else if (meusLacaios.get(j-1).getAtaque() < meusLacaios.get(j).getAtaque()){
								LacaioVivoBuffar = meusLacaios.get(j);
							}
						}
						
						Jogada mag = new Jogada(TipoJogada.MAGIA, card, LacaioVivoBuffar);
						minhasJogadas.add(mag);
						minhaMana -= card.getMana();
						System.out.println("Jogada: Olha essa magia na sua cara "+ card);
						mao.remove(i);
						i--;
					}
				}
			}
		}
		
		//modo de ATAQUE LACAIOS
		for(int i =0; i< meusLacaios.size();i++){
			Carta LacaioVivoAtacar = meusLacaios.get(i);
			Carta LacaioOponente = null;
			int flag = 0, j;
			
			for (j=0; flag == 0 && j<lacaiosOponente.size();j++){
				CartaLacaio lacopo = lacaiosOponente.get(j);
				CartaLacaio lacatk = (CartaLacaio) LacaioVivoAtacar;
				
				/*condições para realizar o ataque:
				 * 1- posso matar o Lacaio
				 * 		1.1-sobrevivo
				 * 		1.2-não sobrevivo
				 * 			1.2.1-o custo de mana
				 * 			1.2.2-tenho dano e a minha vida menor
				 */
				
				if (lacatk.getAtaque() >= lacopo.getVidaAtual() ){
					if (lacatk.getVidaAtual() > lacopo.getAtaque()){
						flag = 1;
					}
					else if(lacatk.getVidaAtual() <= lacopo.getAtaque() ){
						if (lacatk.getMana() < lacopo.getMana() ){
							flag = 1;
						}
						else if ((lacatk.getVidaMaxima() -lacatk.getVidaAtual() != 0) && (lacatk.getVidaAtual() < lacopo.getVidaAtual())){
							flag = 1;
						}
					}
				}
			}
			
			//Atacar o Herói
			if (flag == 0){
				System.out.println("Lacaio "+ LacaioVivoAtacar.getNome() +" ao ATAQUEE!");
				Jogada lacvivo = new Jogada(TipoJogada.ATAQUE, LacaioVivoAtacar, null);
				minhasJogadas.add(lacvivo);
			}
			//Atacar o Lacaio do oponente
			else if (flag == 1){
				j--;
				flag = 0;
				LacaioOponente = lacaiosOponente.get(j);
				lacaiosOponente.remove(j);
				System.out.println("Lacaio "+ LacaioVivoAtacar.getNome() +" ao ATAQUEE!");
				Jogada lacvivo = new Jogada(TipoJogada.ATAQUE, LacaioVivoAtacar, LacaioOponente);
				minhasJogadas.add(lacvivo);
			}
		}
		
		//O HERÓI decide atacar um lacaio ou o HERÓI inimigo
		if (minhaMana >= 2){
			Carta LacaioOponente = null;
			
			for (int j=0;j<lacaiosOponente.size();j++){
				CartaLacaio lacopo = lacaiosOponente.get(j);
				
				if (lacopo.getVidaAtual() == 1 && lacopo.getAtaque() < minhaVida ){
					LacaioOponente =  lacopo;
				} 
			}
			
			Jogada pod = new Jogada(TipoJogada.PODER, null, LacaioOponente);
			minhasJogadas.add(pod);
			minhaMana -= 2;
		}
		
		return minhasJogadas;
	}
}