import java.util.ArrayList;
import java.util.Random;

/**
* Esta classe representa um Jogador aleatório (realiza jogadas de maneira aleatória) para o jogo LaMa (Lacaios & Magias).
* @see java.lang.Object
* @author Matheus Mendes Araujo - MC302
* 
* Para a implementação da estratégia, eu não levei em consideração as cartas que ainda estavam por vir, eu considerei apenas
* as cartas em mãos, através do custo de mana, pois ele é diretamente proporcional ao poder e a força da carta. Não avaliei 
* com nenhum tipo de memória as cartas baixadas pelo oponente. A troca de comportamentos estão detlahadas na implementação,
* aqui apenas listarei os atributos que levei em consideração para cada comportamento :
* 	Agressivo:Mana, ataque dos meus Lacaios, Vida do Oponente
* 	Controle: Mana ,qtde cartas do oponente, Total do Ataque dos Lacaios do ponente, qtde Lacaios
* 	Curva de Mana: Mana, Total do Ataque dos Lacaios do ponente, qtde Lacaios
*Através da fórmulas denotadas nos método EscolhaModos(), eu aplico um peso em cada um dos parâmetros e realizo uma comparação
*para saber qual estratégia, eu devo escolher em cada uma das rodadas. De início, eu posso utilizar a quantidade de mana para 
*saber em qual parte do jogo me encontro, sendo ínicio, meio, ou fim. 
*	Algo em comum das estratégias:
*		O Uso do poder heróico caso sobre, no mínimo 2 de mana, a estatégia é atacar um lacaio com apenas 1 de vida ou o herói 
*diretamente, sendo elevado em consideração o ataque do Lacaio e a vida do meu Herói.
*	Por algumas análises, eu pude perceber que o comportamento agressivo tem uma taxa maior de vitória, em relação aos outros,
*no modo de escolha, eu fiz com que o modo agressivo tivesse algumas vantagens perante os outros, para ser mais escolhido, no
*meio do jogo, onde poderá atuar com frente direta ao herói do oponente.
*	Comportamentos de Jogo:
*		Agressivo:
*			1-Ordena os lacaios por ordem de ataque, o mais forte no ínicio
*			2-Tenta Invocar o primeiro Lacaio mais forte, até o mana acabar
*			3-Tenta Invocar alguma magia
*			4-Manda os Lacaios Atacar o Herói do Oponente
*			5-poder heróico
*		Controle:
*			1-Ordena os lacaios por ordem de mana,do menor mana para o maior, com critério de desempate no ataque
*			2-Tenta invocar um lacaio com o custo total do round
*			3-Usa as magias de acordo:
*				3.1 Alvo, se mata ou deixa o lacaio com 1 de vida
*				3.2 Area, se tiver no mínimo 2 lacaios
*				3.3 Buff, no lacaio mais forte
*			4-Manda os Lacaios atacar
*				4.1 posso matar o Lacaio
*				4.2 não sobrevivo
*					4.2.1 o custo de mana
*					4.2.2 tenho dano e a minha vida menor
*				4.3 se não, ataca o herói
*			5- pode heróico
*		Curva de Mana:
*			1-Ordena os lacaios por ordem de mana,do menor mana para o maior, com critério de desempate no ataque
*			2-Tenta invocar um lacaio com custo do round, do mais forte para o mais fraco
*			3-Usa as magias de acordo:
*				3.1 Alvo, se mata ou deixa o lacaio com 1 de vida
*				3.2 Area, se tiver no mínimo 2 lacaios
*				3.3 Buff, no lacaio mais forte
*			4-Manda os Lacaios atacar
*				4.1 posso matar o Lacaio
*				4.2 não sobrevivo
*					4.2.1 o custo de mana
*					4.2.2 tenho dano e a minha vida menor
*				4.3 se não, ataca o herói
*			5- pode heróico
*			
*/
public class JogadorRA156737 extends Jogador {
	private ArrayList<CartaLacaio> lacaios;
	private ArrayList<CartaLacaio> lacaiosOponente;
	
	/**
	  * O método construtor do JogadorAleatorio.
	  * 
	  * @param maoInicial Contém a mão inicial do jogador. Deve conter o número de cartas correto dependendo se esta classe Jogador que está sendo construída é o primeiro ou o segundo jogador da partida. 
	  * @param primeiro   Informa se esta classe Jogador que está sendo construída é o primeiro jogador a iniciar nesta jogada (true) ou se é o segundo jogador (false).
	  */
	public JogadorRA156737(ArrayList<Carta> maoInicial, boolean primeiro){
		primeiroJogador = primeiro;
		
		mao = maoInicial;
		lacaios = new ArrayList<CartaLacaio>();
		lacaiosOponente = new ArrayList<CartaLacaio>();
		
		// Mensagens de depuração:
		System.out.println("*Classe JogadorRA156737* Sou o " + (primeiro?"primeiro":"segundo") + " jogador (classe: JogadorAleatorio)");
		System.out.println("Mao inicial:");
		for(int i = 0; i < mao.size(); i++)
			System.out.println("ID " + mao.get(i).getID() + ": " + mao.get(i));
	}
	
	/**
	  * Um método que processa o turno de cada jogador. Este método deve retornar as jogadas do Jogador decididas para o turno atual (ArrayList de Jogada).
	  * 
	  * @param mesa   O "estado do jogo" imediatamente antes do início do turno corrente. Este objeto de mesa contém todas as informações 'públicas' do jogo (lacaios vivos e suas vidas, vida dos heróis, etc).
	  * @param cartaComprada   A carta que o Jogador recebeu neste turno (comprada do Baralho). Obs: pode ser null se o Baralho estiver vazio ou o Jogador possuir mais de 10 cartas na mão.
	  * @param jogadasOponente   Um ArrayList de Jogada que foram os movimentos utilizados pelo oponente no último turno, em ordem.
	  * @return            um ArrayList com as Jogadas decididas
	  */
	public ArrayList<Jogada> processarTurno (Mesa mesa, Carta cartaComprada, ArrayList<Jogada> jogadasOponente){
		int minhaMana, minhaVida;
		int opoMana, opoVida;
		int minhaCartas, opoCartas; 
		int escolhidoModo = 0;
		
		if(cartaComprada != null)
			mao.add(cartaComprada);
		if(primeiroJogador){
			minhaMana = mesa.getManaJog1();
			minhaVida = mesa.getVidaHeroi1();
			minhaCartas = mesa.getNumCartasJog1();
			opoMana = mesa.getManaJog2();
			opoVida = mesa.getVidaHeroi2();
			opoCartas = mesa.getNumCartasJog2();
			lacaios = mesa.getLacaiosJog1();
			lacaiosOponente = mesa.getLacaiosJog2();
			//System.out.println("--------------------------------- Começo de turno pro jogador1");
		}
		else{
			minhaMana = mesa.getManaJog2();
			minhaVida = mesa.getVidaHeroi2();
			minhaCartas = mesa.getNumCartasJog2();
			opoMana = mesa.getManaJog1();
			opoVida = mesa.getVidaHeroi1();
			opoCartas = mesa.getNumCartasJog1();
			lacaios = mesa.getLacaiosJog2();
			lacaiosOponente = mesa.getLacaiosJog1();
			//System.out.println("--------------------------------- Começo de turno pro jogador2");
		}
		
		ArrayList<Jogada> minhasJogadas = new ArrayList<Jogada>();
		ArrayList<CartaLacaio> meusLacaios = primeiroJogador ? mesa.getLacaiosJog1() : mesa.getLacaiosJog2();
		ArrayList<Carta> LacaiosOrdemForça = new ArrayList<Carta>();
		ArrayList<Carta> LacaiosOrdemMana = new ArrayList<Carta>();
		
		//escolhe o comportamento do jogador
		escolhidoModo = EscolhaModo (minhaMana, minhaVida, minhaCartas, opoMana, opoVida, opoCartas,  meusLacaios );
		
		if ( escolhidoModo == 0 ){
			minhasJogadas = Controle ( minhaMana, minhaVida, minhasJogadas,  meusLacaios, LacaiosOrdemMana );
		}
		else if ( escolhidoModo == 1  ){
			minhasJogadas = Agressive ( minhaMana, minhaVida, minhasJogadas,  meusLacaios, LacaiosOrdemForça );
		}
		else if ( escolhidoModo == 2  ){
			minhasJogadas = CurvaMana( minhaMana, minhaVida, minhasJogadas, meusLacaios, LacaiosOrdemMana );
		}
		return minhasJogadas;
	}
	
	public int CustoTodosLacaiosOp( ArrayList<CartaLacaio> lacaiosOponente){
		int custoTotal = 0;
		
		for(int i = 0; i<lacaiosOponente.size(); i++){
			CartaLacaio lacopo = lacaiosOponente.get(i);
			custoTotal += lacopo.getMana();
		}
		
		return custoTotal;
	}
	
	public int AtaqueTodosLacaios( ArrayList<CartaLacaio> lacaios){
		int AtaqueLacaios = 0;
		
		for(int i = 0; i<lacaios.size(); i++){
			CartaLacaio lacopo = lacaios.get(i);
			AtaqueLacaios += lacopo.getAtaque();
		}
		
		return AtaqueLacaios;
	}
	
	public ArrayList<Jogada> Controle ( int minhaMana, int minhaVida, ArrayList<Jogada> minhasJogadas, 
			ArrayList<CartaLacaio> meusLacaios, ArrayList<Carta> LacaiosOrdemMana ){
		
		// O laço abaixo cria jogadas de baixar LACAIOS se houver mana disponível.
		//laço de selecionar os lacaios mais fortes na frente
		for (int i = 0; i < mao.size(); i++){
			Carta card = mao.get(i);
			if (i == 0){
				if (card instanceof CartaLacaio){
					LacaiosOrdemMana.add(card);
				}
			}
			else{
				if (card instanceof CartaLacaio){
					for (int j = 0, flag = 0; j < LacaiosOrdemMana.size() && flag == 0; j++){
						CartaLacaio lac1 =  (CartaLacaio) LacaiosOrdemMana.get(j);
						CartaLacaio lac2 =  (CartaLacaio) card;
						
						if (lac1.getMana() > lac2.getMana()){
							LacaiosOrdemMana.add(j, card);
							flag = 1;
						}
						else if (lac1.getMana() == lac2.getMana()){
							if (lac1.getAtaque() <= lac2.getAtaque()){
								LacaiosOrdemMana.add(j, card);
							}
							else {
								LacaiosOrdemMana.add(j+1, card);
							}
							flag = 1;
						}
						else if (j == LacaiosOrdemMana.size() - 1 ){
							LacaiosOrdemMana.add(card);
							flag = 1;
						}
					}
				}
			}
		}
		
		//invocar o lacaio
		for (int i = 0; i < LacaiosOrdemMana.size(); i++){
			Carta card = LacaiosOrdemMana.get(i);
			if (meusLacaios.size() < 7){
				//usar carta LACAIO
				if(card instanceof CartaLacaio && card.getMana() <= minhaMana){
					Jogada lac = new Jogada(TipoJogada.LACAIO, card, null);
					minhasJogadas.add(lac);
					minhaMana -= card.getMana();
					System.out.println("Jogada: Decidi baixar o lacaio: "+ card);
					
					for (int j = 0, flag = 0; j < mao.size() && flag == 0; j++){
						if (mao.get(j).getID() == card.getID()){
							mao.remove(j);
							flag = 1;
						}
					}
					LacaiosOrdemMana.remove(i);
					i--;
				}
			}
		}
		
		// O laço abaixo cria jogadas de baixar MAGIAAAS se houver mana disponível.
		for(int i = 0; i < mao.size(); i++){
			Carta card = mao.get(i);
			
			//usar carta MAGIA
			if(card instanceof CartaMagia && card.getMana() <= minhaMana){
				CartaMagia magia = (CartaMagia) card;
				
				if (magia.getMagiaTipo() == TipoMagia.ALVO){
					for (int j = 0,flag = 0; j < lacaiosOponente.size() && flag == 0; j++){
						Carta LacaioOp = lacaiosOponente.get(j);
						CartaLacaio lac1 = (CartaLacaio) LacaioOp;
						if (Math.abs(magia.getMagiaDano() - lac1.getVidaAtual()) == 1 || magia.getMagiaDano() - lac1.getVidaAtual() == 0 ){
							Jogada mag = new Jogada(TipoJogada.MAGIA, card, null);
							minhasJogadas.add(mag);
							minhaMana -= card.getMana();
							if (magia.getMagiaDano() - lac1.getVidaAtual() >= 0 ){
								lacaiosOponente.remove(j);
							}
							System.out.println("Jogada: Olha essa magia na sua cara "+ card);
							mao.remove(i);
							i--;
							flag = 1;
						}
					}
				}
				else if(magia.getMagiaTipo() == TipoMagia.AREA && lacaiosOponente.size() >= 2){
					Jogada mag = new Jogada(TipoJogada.MAGIA, card, null);
					minhasJogadas.add(mag);
					minhaMana -= card.getMana();
					System.out.println("Jogada: Olha essa magia na sua cara "+ card);
					mao.remove(i);
					i--;
					
					//Verifica quem foi eliminado para remover
					for (int j = 0; j < lacaiosOponente.size(); j++){
						if ( magia.getMagiaDano() - lacaiosOponente.get(j).getVidaAtual() >= 0){
							lacaiosOponente.remove(j);
							j--;
						}
					}
					
				}
				else if (magia.getMagiaTipo() == TipoMagia.BUFF){
					if (meusLacaios.size() != 0){
						Carta LacaioVivoBuffar = meusLacaios.get(0);
						for (int j =0; j<meusLacaios.size();j++){
							if (j ==0){
								LacaioVivoBuffar = meusLacaios.get(j);
							}
							else if (meusLacaios.get(j-1).getAtaque() < meusLacaios.get(j).getAtaque()){
								LacaioVivoBuffar = meusLacaios.get(j);
							}
						}
						
						Jogada mag = new Jogada(TipoJogada.MAGIA, card, LacaioVivoBuffar);
						minhasJogadas.add(mag);
						minhaMana -= card.getMana();
						System.out.println("Jogada: Olha essa magia na sua cara "+ card);
						mao.remove(i);
						i--;
					}
				}
			}
		}
		
		//modo de ATAQUE LACAIOS
		for(int i =0; i< meusLacaios.size();i++){
			Carta LacaioVivoAtacar = meusLacaios.get(i);
			Carta LacaioOponente = null;
			int flag = 0, j;
			
			for (j=0; flag == 0 && j<lacaiosOponente.size();j++){
				CartaLacaio lacopo = lacaiosOponente.get(j);
				CartaLacaio lacatk = (CartaLacaio) LacaioVivoAtacar;
				
				/*condições para realizar o ataque:
				 * 1- posso matar o Lacaio
				 * 		1.1-sobrevivo
				 * 		1.2-não sobrevivo
				 * 			1.2.1-o custo me mana
				 * 			1.2.2-tenho dano e a minha vida menor
				 */
				
				if (lacatk.getAtaque() >= lacopo.getVidaAtual() ){
					if (lacatk.getVidaAtual() > lacopo.getAtaque()){
						flag = 1;
					}
					else if(lacatk.getVidaAtual() <= lacopo.getAtaque() ){
						if (lacatk.getMana() < lacopo.getMana() ){
							flag = 1;
						}
						else if ((lacatk.getVidaMaxima() -lacatk.getVidaAtual() != 0) && (lacatk.getVidaAtual() < lacopo.getVidaAtual())){
							flag = 1;
						}
					}
				}
			}
			
			//Atacar o Herói
			if (flag == 0){
				System.out.println("Lacaio "+ LacaioVivoAtacar.getNome() +" ao ATAQUEE!");
				Jogada lacvivo = new Jogada(TipoJogada.ATAQUE, LacaioVivoAtacar, null);
				minhasJogadas.add(lacvivo);
			}
			//Atacar o Lacaio do oponente
			else if (flag == 1){
				j--;
				flag = 0;
				LacaioOponente = lacaiosOponente.get(j);
				lacaiosOponente.remove(j);
				System.out.println("Lacaio "+ LacaioVivoAtacar.getNome() +" ao ATAQUEE!");
				Jogada lacvivo = new Jogada(TipoJogada.ATAQUE, LacaioVivoAtacar, LacaioOponente);
				minhasJogadas.add(lacvivo);
			}
		}
		
		//O HERÓI decide atacar um lacaio ou o HERÓI inimigo
		if (minhaMana >= 2){
			Carta LacaioOponente = null;
			
			for (int j=0;j<lacaiosOponente.size();j++){
				CartaLacaio lacopo = lacaiosOponente.get(j);
				
				if (lacopo.getVidaAtual() == 1 && lacopo.getAtaque() < minhaVida ){
					LacaioOponente =  lacopo;
				} 
			}
			
			Jogada pod = new Jogada(TipoJogada.PODER, null, LacaioOponente);
			minhasJogadas.add(pod);
			minhaMana -= 2;
		}
		
		return minhasJogadas;
	}
	
	public ArrayList<Jogada> Agressive ( int minhaMana, int minhaVida, ArrayList<Jogada> minhasJogadas, 
			ArrayList<CartaLacaio> meusLacaios, ArrayList<Carta> LacaiosOrdemForça ){
		
		// O laço abaixo cria jogadas de baixar LACAIOS se houver mana disponível.
		//laço de selecionar os lacaios mais fortes na frente
		for (int i = 0; i < mao.size(); i++){
			Carta card = mao.get(i);
			if (i == 0){
				if (card instanceof CartaLacaio){
					LacaiosOrdemForça.add(card);
				}
			}
			else{
				if (card instanceof CartaLacaio){
					for (int j = 0, flag = 0; j < LacaiosOrdemForça.size() && flag == 0; j++){
						CartaLacaio lac1 =  (CartaLacaio) LacaiosOrdemForça.get(j);
						CartaLacaio lac2 =  (CartaLacaio) card;
						
						if (lac1.getAtaque() < lac2.getAtaque()){
							LacaiosOrdemForça.add(j, card);
							flag = 1;
						}
						else if (lac1.getAtaque() == lac2.getAtaque()){
							if (LacaiosOrdemForça.get(j).getMana() >= card.getMana()){
								LacaiosOrdemForça.add(j, card);
							}
							else {
								LacaiosOrdemForça.add(j+1, card);
							}
							flag = 1;
						}
						else if (j == LacaiosOrdemForça.size() - 1 ){
							LacaiosOrdemForça.add(card);
							flag = 1;
						}
					}
				}
			}
		}
		
		//invocar o lacaio
		for (int i = 0; i < LacaiosOrdemForça.size(); i++){
			Carta card = LacaiosOrdemForça.get(i);
			if (meusLacaios.size() < 7){
				//usar carta LACAIO
				if(card instanceof CartaLacaio && card.getMana() <= minhaMana){
					Jogada lac = new Jogada(TipoJogada.LACAIO, card, null);
					minhasJogadas.add(lac);
					minhaMana -= card.getMana();
					System.out.println("Jogada: Decidi baixar o lacaio: "+ card);
					
					for (int j = 0, flag = 0; j < mao.size() && flag == 0; j++){
						if (mao.get(j).getID() == card.getID()){
							mao.remove(j);
							flag = 1;
						}
					}
					LacaiosOrdemForça.remove(i);
					i--;
				}
			}
		}
		
		// O laço abaixo cria jogadas de baixar MAGIAAAS se houver mana disponível.
		for(int i = 0; i < mao.size(); i++){
			Carta card = mao.get(i);
			
			//usar carta MAGIA
			if(card instanceof CartaMagia && card.getMana() <= minhaMana){
				CartaMagia magia = (CartaMagia) card;
				
				if (magia.getMagiaTipo() == TipoMagia.ALVO){
					for (int j = 0,flag = 0; j < lacaiosOponente.size() && flag == 0; j++){
						Carta LacaioOp = lacaiosOponente.get(j);
						CartaLacaio lac1 = (CartaLacaio) LacaioOp;
						if (Math.abs(magia.getMagiaDano() - lac1.getVidaAtual()) == 1 || magia.getMagiaDano() - lac1.getVidaAtual() == 0 ){
							Jogada mag = new Jogada(TipoJogada.MAGIA, card, null);
							minhasJogadas.add(mag);
							minhaMana -= card.getMana();
							if (magia.getMagiaDano() - lac1.getVidaAtual() >= 0 ){
								lacaiosOponente.remove(j);
							}
							System.out.println("Jogada: Olha essa magia na sua cara "+ card);
							mao.remove(i);
							i--;
							flag = 1;
						}
					}
				}
				else if(magia.getMagiaTipo() == TipoMagia.AREA && lacaiosOponente.size() >= 2){
					Jogada mag = new Jogada(TipoJogada.MAGIA, card, null);
					minhasJogadas.add(mag);
					minhaMana -= card.getMana();
					System.out.println("Jogada: Olha essa magia na sua cara "+ card);
					mao.remove(i);
					i--;
					
					//Verifica quem foi eliminado para remover
					for (int j = 0; j < lacaiosOponente.size(); j++){
						if ( magia.getMagiaDano() - lacaiosOponente.get(j).getVidaAtual() >= 0){
							lacaiosOponente.remove(j);
							j--;
						}
					}
					
				}
				else if (magia.getMagiaTipo() == TipoMagia.BUFF){
					if (meusLacaios.size() != 0){
						Carta LacaioVivoBuffar = meusLacaios.get(0);
						Jogada mag = new Jogada(TipoJogada.MAGIA, card, LacaioVivoBuffar);
						minhasJogadas.add(mag);
						minhaMana -= card.getMana();
						System.out.println("Jogada: Olha essa magia na sua cara "+ card);
						mao.remove(i);
						i--;
					}
				}
			}
		}
		
		//modo de ATAQUE LACAIOS
		for(int j =0; j< meusLacaios.size();j++){
			Carta LacaioVivoAtacar = meusLacaios.get(j);
			System.out.println("Lacaio "+ LacaioVivoAtacar.getNome() +" ao ATAQUEE!");
			Jogada lacvivo = new Jogada(TipoJogada.ATAQUE, LacaioVivoAtacar, null);
			minhasJogadas.add(lacvivo);
		}
		
		//O HERÓI decide atacar um lacaio ou o HERÓI inimigo
		if (minhaMana >= 2){
			Carta LacaioOponente = null;
			
			for (int j=0;j<lacaiosOponente.size();j++){
				CartaLacaio lacopo = lacaiosOponente.get(j);
				
				if (lacopo.getVidaAtual() == 1 && lacopo.getAtaque() < minhaVida
						&& AtaqueTodosLacaios(lacaiosOponente) < minhaVida/3){
					LacaioOponente =  lacopo;
				} 
			}
			
			Jogada pod = new Jogada(TipoJogada.PODER, null, LacaioOponente);
			minhasJogadas.add(pod);
			minhaMana -= 2;
		}
		
		return minhasJogadas;
	}
	
	public ArrayList<Jogada> CurvaMana( int minhaMana, int minhaVida, ArrayList<Jogada> minhasJogadas, 
			ArrayList<CartaLacaio> meusLacaios, ArrayList<Carta> LacaiosOrdemMana ){
		/**
		 *  O laço abaixo cria jogadas de baixar LACAIOS se houver mana disponível.
		 *  laço de selecionar os lacaios mais fortes na frente, ordem de mana
		 */
		for (int i = 0; i < mao.size(); i++){
			Carta card = mao.get(i);
			if (i == 0){
				if (card instanceof CartaLacaio){
					LacaiosOrdemMana.add(card);
				}
			}
			else{
				if (card instanceof CartaLacaio){
					for (int j = 0, flag = 0; j < LacaiosOrdemMana.size() && flag == 0; j++){
						CartaLacaio lac1 =  (CartaLacaio) LacaiosOrdemMana.get(j);
						CartaLacaio lac2 =  (CartaLacaio) card;
						
						if (lac1.getMana() > lac2.getMana()){
							LacaiosOrdemMana.add(j, card);
							flag = 1;
						}
						else if (lac1.getMana() == lac2.getMana()){
							if (lac1.getAtaque() <= lac2.getAtaque()){
								LacaiosOrdemMana.add(j, card);
							}
							else {
								LacaiosOrdemMana.add(j+1, card);
							}
							flag = 1;
						}
						else if (j == LacaiosOrdemMana.size() - 1 ){
							LacaiosOrdemMana.add(card);
							flag = 1;
						}
					}
				}
			}
		}
		
		//invocar o lacaio
		for (int i = LacaiosOrdemMana.size() -1; i >= 0; i--){
			Carta card = LacaiosOrdemMana.get(i);
			if (meusLacaios.size() < 7){
				//usar carta LACAIO
				if(card instanceof CartaLacaio && card.getMana() <= minhaMana){
					Jogada lac = new Jogada(TipoJogada.LACAIO, card, null);
					minhasJogadas.add(lac);
					minhaMana -= card.getMana();
					System.out.println("Jogada: Decidi baixar o lacaio: "+ card);
					
					for (int j = 0, flag = 0; j < mao.size() && flag == 0; j++){
						if (mao.get(j).getID() == card.getID()){
							mao.remove(j);
							flag = 1;
						}
					}
					LacaiosOrdemMana.remove(i);
					i--;
				}
			}
		}
		
		// O laço abaixo cria jogadas de baixar MAGIAAAS se houver mana disponível.
		for(int i = 0; i < mao.size(); i++){
			Carta card = mao.get(i);
			
			//usar carta MAGIA
			if(card instanceof CartaMagia && card.getMana() <= minhaMana){
				CartaMagia magia = (CartaMagia) card;
				
				if (magia.getMagiaTipo() == TipoMagia.ALVO){
					for (int j = 0,flag = 0; j < lacaiosOponente.size() && flag == 0; j++){
						Carta LacaioOp = lacaiosOponente.get(j);
						CartaLacaio lac1 = (CartaLacaio) LacaioOp;
						if ((magia.getMagiaDano() - lac1.getVidaAtual() >= 0) && (magia.getMana() <= lac1.getMana())){
							Jogada mag = new Jogada(TipoJogada.MAGIA, card, null);
							minhasJogadas.add(mag);
							minhaMana -= card.getMana();
							if (magia.getMagiaDano() - lac1.getVidaAtual() >= 0 ){
								lacaiosOponente.remove(j);
							}
							System.out.println("Jogada: Olha essa magia na sua cara "+ card);
							mao.remove(i);
							i--;
							flag = 1;
						}
					}
				}
				else if(magia.getMagiaTipo() == TipoMagia.AREA && magia.getMana() >= CustoTodosLacaiosOp(lacaiosOponente)){
					Jogada mag = new Jogada(TipoJogada.MAGIA, card, null);
					minhasJogadas.add(mag);
					minhaMana -= card.getMana();
					System.out.println("Jogada: Olha essa magia na sua cara "+ card);
					mao.remove(i);
					i--;
					
					for (int j = 0; j < lacaiosOponente.size(); j++){
						if ( magia.getMagiaDano() - lacaiosOponente.get(j).getVidaAtual() >= 0){
							lacaiosOponente.remove(j);
							j--;
						}
					}
					
				}
				else if (magia.getMagiaTipo() == TipoMagia.BUFF){
					if (meusLacaios.size() != 0){
						Carta LacaioVivoBuffar = meusLacaios.get(0);
						Jogada mag = new Jogada(TipoJogada.MAGIA, card, LacaioVivoBuffar);
						minhasJogadas.add(mag);
						minhaMana -= card.getMana();
						System.out.println("Jogada: Olha essa magia na sua cara "+ card);
						mao.remove(i);
						i--;
					}
				}
			}
		}
		
		//modo de ATAQUE LACAIOS
		for(int i =0; i< meusLacaios.size();i++){
			Carta LacaioVivoAtacar = meusLacaios.get(i);
			Carta LacaioOponente = null;
			int flag = 0, j;
			
			for (j=0; flag == 0 && j<lacaiosOponente.size();j++){
				CartaLacaio lacopo = lacaiosOponente.get(j);
				CartaLacaio lacatk = (CartaLacaio) LacaioVivoAtacar;
				
				/**condições para realizar o ataque:
				 * 1- posso matar o Lacaio
				 * 		1.1-sobrevivo
				 * 		1.2-não sobrevivo
				 * 			1.2.1-o custo me mana
				 * 			1.2.2-tenho dano e a minha vida menor
				 **/
				
				if (lacatk.getAtaque() >= lacopo.getVidaAtual() ){
					if (lacatk.getVidaAtual() > lacopo.getAtaque()){
						flag = 1;
					}
					else if(lacatk.getVidaAtual() <= lacopo.getAtaque() ){
						if (lacatk.getMana() < lacopo.getMana() ){
							flag = 1;
						}
						else if ((lacatk.getVidaMaxima() -lacatk.getVidaAtual() != 0) && (lacatk.getVidaAtual() < lacopo.getVidaAtual())){
							flag = 1;
						}
					}
				}
			}
			
			//Atacar o Herói
			if (flag == 0){
				System.out.println("Lacaio "+ LacaioVivoAtacar.getNome() +" ao ATAQUEE!");
				Jogada lacvivo = new Jogada(TipoJogada.ATAQUE, LacaioVivoAtacar, null);
				minhasJogadas.add(lacvivo);
			}
			//Atacar o Lacaio do oponente
			else if (flag == 1){
				j--;
				flag = 0;
				LacaioOponente = lacaiosOponente.get(j);
				lacaiosOponente.remove(j);
				System.out.println("Lacaio "+ LacaioVivoAtacar.getNome() +" ao ATAQUEE!");
				Jogada lacvivo = new Jogada(TipoJogada.ATAQUE, LacaioVivoAtacar, LacaioOponente);
				minhasJogadas.add(lacvivo);
				
			}
		}
		
		//O HERÓI decide atacar um lacaio ou o HERÓI inimigo
		if (minhaMana >= 2){
			Carta LacaioOponente = null;
			
			for (int j=0;j<lacaiosOponente.size();j++){
				CartaLacaio lacopo = lacaiosOponente.get(j);
				
				if (lacopo.getVidaAtual() == 1 && lacopo.getAtaque() < minhaVida ){
					LacaioOponente =  lacopo;
				} 
			}
			
			Jogada pod = new Jogada(TipoJogada.PODER, null, LacaioOponente);
			minhasJogadas.add(pod);
			minhaMana -= 2;
		}
		
		return minhasJogadas;
	}
	
	public int EscolhaModo ( int minhaMana, int minhaVida, int minhaCartas, int opoMana, int opoVida, int opoCartas, 
			ArrayList<CartaLacaio> meusLacaios   ){
		//Em cada uma das fórmulas foi usada a média ponderada
		int flag = 0;
		int meuLacAtk = AtaqueTodosLacaios(meusLacaios);
		int opoLacAtk = AtaqueTodosLacaios(lacaiosOponente);
		// valor de escolha modo agressive e os pesos
		double agressive;
		double pmA = 2.5, pvoA= 3.5 , patA = 3.5;
		// valor de escolha modo Controle e os pesos
		double controle;
		double pmC = 1, patC = 3 , qtdeiC = 5, qtdecC = 1 ;
		// valor de escolha modo Curva Mana e os pesos
		double curvaMana;
		double pmM = 4, patM = 3 , qtdeiM = 3;		
		
		/*System.out.println("--------------------------------- AGRESSIVEEEEEEEEEEEEEE");
		System.out.println("MANA "+ minhaMana);
		System.out.println("V. Oponente "+ opoVida);
		System.out.println("LacAtk "+ meuLacAtk);*/
		
		agressive = (double)(((minhaMana*pmA)+(meuLacAtk*patA)+((30 - opoVida)*pvoA*0.75))/
				(pmA + pvoA + patA));
		
		/*System.out.println("Numero "+ agressive);
		System.out.println("--------------------------------- AGRESSIVEEEEEEEEEEEEEE");
		
		System.out.println("--------------------------------- CONTROLEEEEEEE");
		System.out.println("MANA "+ minhaMana);
		System.out.println("V. Oponente "+ opoVida);
		System.out.println("OpoLacAtk "+ opoLacAtk);
		System.out.println("Qtde Lacaio "+ lacaios.size());*/
		
		controle = (double)(((minhaMana*pmC)+(opoCartas*qtdecC)+(opoLacAtk*patC)+ lacaiosOponente.size()*qtdeiC)/
				(pmC + qtdeiC + patC + qtdecC));
		
		/*System.out.println("Numero "+ controle);
		System.out.println("--------------------------------- CONTROLEEEEEEE");
		
		System.out.println("--------------------------------- MANAAAAAAAAAAAA");
		System.out.println("MANA "+ minhaMana);
		System.out.println("V. Oponente "+ opoVida);
		System.out.println("OpoLacAtk "+ opoLacAtk);
		System.out.println("Qtde Lacaio "+ lacaios.size());*/
		
		curvaMana = (double)((((6 -minhaMana)*pmM)+(opoLacAtk*patM)+lacaiosOponente.size()*qtdeiM)/
				(pmM + qtdeiM + patM));
		
		/*System.out.println("Numero "+ curvaMana);
		System.out.println("--------------------------------- MANAAAAAAAAAAAA");*/
		
		if (agressive >= controle  && agressive > curvaMana){
			flag = 1;
			//System.out.println("--------------------------------- ESCOLHIIIIIIIIIIIIIII AGRESSIVEEEEEEEEE");
		}
		else if (curvaMana > agressive && curvaMana >= controle){
			flag = 2;
			//System.out.println("--------------------------------- ESCOLHIIIIIIIIIIIIIII MANAAAAAAAAAAAAA");
		}
		else{
			//System.out.println("--------------------------------- ESCOLHIIIIIIIIIIIIIII CONTROLEEEEEEEEE");
		}
		
		return flag;
	}
	
}